from scrapy.item import Item, Field

class Post(Item):
    author = Field()
    time = Field()
    subject = Field()
    text = Field()

