from pdb import set_trace as st

from os import path

from scrapy.spider import Spider
from scrapy.contrib.spiders.init import InitSpider
from scrapy.contrib.spiders import CrawlSpider
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule

LOGIN_CONF_PATH = path.join(path.dirname(path.abspath(__file__)),
                            'login.conf')
f = open(LOGIN_CONF_PATH)
_LOGIN_CONF = dict([stripped_line.split(':') for
                    stripped_line in [line.strip() for line in f.readlines()]])
f.close()
username = _LOGIN_CONF['username']
password = _LOGIN_CONF['password']
domain = _LOGIN_CONF['domain']

START_CONF_PATH = path.join(path.dirname(path.abspath(__file__)),
                            'start.conf')
f = open(START_CONF_PATH)
START_CONF = filter(lambda line:
                        line[0] == '#',
                    [line.strip() for line in f.readlines()])
f.close()

class PhpBbSpider(CrawlSpider):
    name = 'phpbbspider'
    allowed_domains = [domain]
    login_page = 'http://'+domain+'/'
    start_urls = ['http://'+domain+'/',
                  ]
    rules = (
        Rule(SgmlLinkExtractor(allow=r''),
        # Rule(SgmlLinkExtractor(allow=r'-\w+.html$'),
             callback='parse_item', follow=True),
    )

    def start_requests(self):
        yield Request(
            url=self.login_page,
            callback=self.login,
            dont_filter=True
            )

    def init_request(self):
        """This function is called before crawling starts."""
        return Request(url=self.login_page, callback=self.login)

    def login(self, response):
        """Generate a login request."""
        f = open('login_page.html', 'w')
        f.write(response.body)
        f.close()
        return FormRequest.from_response(response,
                    formdata={'username': username, 'password': password},
                    callback=self.check_login_response)

    def check_login_response(self, response):
        """Check the response returned by a login request to see if we are
        successfully logged in.
        """
        f = open('login_response_page.html', 'w')
        f.write(response.body)
        f.close()

class PhpBbSimpleSpider(CrawlSpider):
    name = 'simplephpbbspider'
    allowed_domains = [domain]
    login_page = 'http://'+domain+'/'
    start_urls = [ 'http://'+domain+'/'+start+'.html'
                   for start in START_CONF
                   ]

    def parse(self, response):
        st()
        pass
